#!/usr/bin/python

import requests
import json
import xml.etree.ElementTree as ET
import codecs

# we set JSON data APJ6JRA9NG5V4
requestMetadata = {'marketplaceID' : 'APJ6JRA9NG5V4' , 'clientID' : 'goldbox_mobile_pc'}
category = {"informatica":"425916031"}
dealMetadata = {
        'requestMetadata': requestMetadata,
        'widgetContext' : {'pageType' : 'Landing','subPageType' : 'hybrid-batch-atf','deviceType' : 'pc','refRID' : '0C4CPSGQHXY6JSW0ER2M','widgetID' : '653378407','slotName' : 'merchandised-search-2'},
        'page' : 1,	'dealsPerPage':300, 'itemResponseSize' : 'NONE',
        'queryProfile':{"dealStates":["AVAILABLE","EXPIRED","SOLDOUT","UPCOMING","WAITLIST","WAITLISTFULL"],
            "includedCategories":[category['informatica']],"excludedExtendedFilters":{"MARKETING_ID":["LowQuality"]},
            "includedBins":[{"count":20,"name":"discount_range"},{"count":20,"name":"price_range"},{"count":100,"name":"whitelist_categories"}]},
        "sortOrder":"BY_DISCOUNT_DESCENDING","version":"V2.2","page":1,"dealsPerPage":300,
        "widgetContext":{"pageType":"GoldBox","subPageType":"main","deviceType":"pc","refRID":"1AMB85NYJFP1AYS5WHF1","widgetID":"820994347","slotName":"slot-5"}
        }



headers = {'Content-type' : 'application/json'}

# first call -> get deal list
metadataRequest = requests.post('http://www.amazon.it/xa/dealcontent/v2/GetDealMetadata', data=json.dumps(dealMetadata), headers=headers)

jsonMetadata = metadataRequest.json();
dealTargets = []


for dealId in jsonMetadata['sortedDealIDs']:
    dealTargets.append({"dealID" : dealId})

dealsData = {
        'requestMetadata': requestMetadata,
        'dealTargets' : dealTargets,
        'responseSize' : 'ALL',
        'itemResponseSize' : 'NONE'
        };

#"itemResponseSize":"DEFAULT_WITH_PREEMPTIVE_LEAKING"}:

# second call -> get available deal details & create RSS
dealsRequest = requests.post('http://www.amazon.it/xa/dealcontent/v2/GetDeals', data=json.dumps(dealsData), headers=headers)

jsonDeals = dealsRequest.json();
#json_string = json.dumps(jsonDeals["dealDetails"], ensure_ascii=False)


i = 0
prds = {"products":[]}
prd = {}
for dealid in jsonDeals["dealDetails"]:
    dealid =  dealid.encode('ascii', 'ignore')
    if jsonDeals["dealDetails"][dealid]["minPercentOff"] is not 0:
        prd["url"] = jsonDeals["dealDetails"][dealid]["egressUrl"]
    	prd["image"] = jsonDeals["dealDetails"][dealid]["primaryImage"]
    	prd["pricemax"] = jsonDeals["dealDetails"][dealid]["maxBAmount"]
    	prd["pricemin"] = jsonDeals["dealDetails"][dealid]["minDealPrice"]
    	prd["percentoff"] = jsonDeals["dealDetails"][dealid]["minPercentOff"] 
    	prd["rating"] = jsonDeals["dealDetails"][dealid]["reviewRating"]

    	prd["description"] = jsonDeals["dealDetails"][dealid]["description"]
    	prd["title"] = jsonDeals["dealDetails"][dealid]["title"]
    	prds["products"].append(prd)   
    	prd = {}
    	dealid = ""


print json.dumps(prds)
exit()
